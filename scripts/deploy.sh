#! /bin/bash

set -e
set -u
set -o pipefail

# Install CDK
npm install -g aws-cdk

# Install Dependencies
npm install
npm run build

# Deploy 
# cdk deploy --require-approval never 
# serverless
# cloudformation